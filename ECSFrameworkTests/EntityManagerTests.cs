﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class EntityManagerTests
    {
        private EntityManager em;

        public EntityManagerTests()
        {
            var s = new State(new NullScheduler());
            var cm = new ComponentTypeManager();
            em = new EntityManager(cm, null, s);
        }

        [Fact]
        public void TestNothingSet()
        {
            var retEntity = em.GetEntity(0);
            var retAll = em.GetEntities(TypeFilter.Empty());
            var retAllCompsA = em.GetComponents(null);
            var retAllCompsB = em.GetComponent<TestRegularComponent>(null);

            Assert.Null(retEntity);
            Assert.Empty(retAll);
            Assert.Null(retAllCompsB);
            Assert.Empty(retAllCompsA);
        }

        [Fact]
        public void TestAddEntity()
        {
            Entity added = null;
            em.WhenEntityAdded.Subscribe(obs =>
            {
                added = obs;
            });

            var newEntity = em.CreateEntity();
            var getTest = em.GetEntity(0);

            Assert.NotNull(added);
            Assert.NotNull(newEntity);
            Assert.NotNull(getTest);
            Assert.Same(added, newEntity);
            Assert.Same(getTest, newEntity);
            Assert.Equal(0, newEntity.ID);
        }

        [Fact]
        public void TestRemoveEntity()
        {
            var newEntity = em.CreateEntity();
            newEntity.AddComponent<TestRegularComponent>();

            var getTest = em.GetEntity(0);

            Entity removed = null;
            em.WhenEntityRemoved.Subscribe(obs =>
            {
                removed = obs;
            });

            Assert.NotNull(newEntity);
            Assert.Null(removed);
            Assert.Equal(0, newEntity.ID);

            em.Remove(newEntity);

            Assert.NotNull(removed);
            Assert.Null(em.GetEntity(0));

            var c = em.GetComponent<TestRegularComponent>(removed);
            Assert.Null(c);
        }

        [Fact]
        public void TestRemoveEntityByID()
        {
            var newEntity = em.CreateEntity();
            newEntity.AddComponent<TestRegularComponent>();

            var getTest = em.GetEntity(0);

            Entity removed = null;
            em.WhenEntityRemoved.Subscribe(obs =>
            {
                removed = obs;
            });

            Assert.NotNull(newEntity);
            Assert.Null(removed);
            Assert.Equal(0, newEntity.ID);

            em.Remove(0);

            Assert.NotNull(removed);
            Assert.Null(em.GetEntity(0));
        }
    }
}
