﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class BitIdentificationTests
    {
        [Fact]
        public void TestCreateBitIdentification()
        {
            BitIdentification newBitAAAA = new BitIdentification(20, 9, 1, 5, 15);
            newBitAAAA.UnsetBit(5);
            var t44441 = newBitAAAA.GetBitIndexes().ToList();

            BitIdentification newBitA = new BitIdentification(10, 0);
            BitIdentification newBitC = new BitIdentification(10, 9);

            BitIdentification newBitG = new BitIdentification(60, 0);
            BitIdentification newBitH = new BitIdentification(60, 40);
            BitIdentification newBitI = new BitIdentification(60, 9);

            var t1 = newBitC.GetBitIndexes().ToList();
            var t2 = newBitH.GetBitIndexes().ToList();
            var t3 = newBitI.GetBitIndexes().ToList();
        }

        [Fact]
        public void TestBitIdentificationAll()
        {
            BitIdentification newBitA = new BitIdentification(10);
            BitIdentification newBitB = new BitIdentification(10);

            newBitA.SetBit(1);
            newBitA.SetBit(5);
            newBitA.SetBit(8);
            newBitA.SetBit(9);

            newBitB.SetBit(5);
            newBitB.SetBit(8);

            bool checkA = newBitA.All(newBitB);
            bool checkB = newBitB.All(newBitA);

            Assert.False(checkA);
            Assert.True(checkB);
        }

        [Fact]
        public void TestBitIdentificationAny()
        {
            BitIdentification newBitA = new BitIdentification(10);
            BitIdentification newBitB = new BitIdentification(10);
            BitIdentification newBitC = new BitIdentification(10);

            newBitA.SetBit(1);
            newBitA.SetBit(5);
            newBitA.SetBit(8);
            newBitA.SetBit(9);

            newBitB.SetBit(5);
            newBitB.SetBit(8);

            newBitC.SetBit(2);
            newBitC.SetBit(3);

            bool checkA = newBitA.Any(newBitB);
            bool checkB = newBitB.Any(newBitA);
            bool checkC = newBitC.Any(newBitA);

            Assert.True(checkA);
            Assert.True(checkB);
            Assert.False(checkC);
        }

        [Fact]
        public void TestBitIdentificationNone()
        {
            BitIdentification newBitA = new BitIdentification(10);
            BitIdentification newBitB = new BitIdentification(10);
            BitIdentification newBitC = new BitIdentification(10);

            newBitA.SetBit(1);
            newBitA.SetBit(5);
            newBitA.SetBit(8);
            newBitA.SetBit(9);

            newBitB.SetBit(5);
            newBitB.SetBit(8);

            newBitC.SetBit(2);
            newBitC.SetBit(3);

            bool checkA = newBitA.None(newBitB);
            bool checkB = newBitB.None(newBitA);
            bool checkC = newBitC.None(newBitA);

            Assert.False(checkA);
            Assert.False(checkB);
            Assert.True(checkC);
        }

        [Fact]
        public void TestBitIdentificationNull()
        {
            BitIdentification newBitA = new BitIdentification(5);
            BitIdentification newBitB = new BitIdentification(105);

            Assert.True(newBitA.Empty);
            Assert.True(newBitB.Empty);

            newBitA.SetBit(1);
            newBitB.SetBit(1);

            Assert.False(newBitA.Empty);
            Assert.False(newBitB.Empty);
        }
    }
}
