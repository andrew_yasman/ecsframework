﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class IntegrationTests
    {
        public IntegrationTests()
        {

        }

        [Fact]
        public void FullRegularSystemCreation()
        {
            EntityWorld ew = new EntityWorld(new NullScheduler());
            //ew.RegisterComponentType<TestRegularComponent>();
            //ew.RegisterComponentType<TestRegularComponentBeta>();
            //ew.RegisterSystem<TestSystemAlpha>();
            //ew.RegisterSystem<TestSystemBeta>();
            ew.Initialize();

            var entity = ew.CreateEntity() as Entity;
            entity.AddComponent<TestRegularComponent>();

            var newComponent = entity.GetComponent<TestRegularComponent>();

        }
    }
}
