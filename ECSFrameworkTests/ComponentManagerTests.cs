﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public struct TestRegularComponent : IComponent { }
    public struct TestRegularComponentBeta : IComponent { }
    public struct TestSharedComponent : ISharedComponent { }
    public struct TestNotComponent { }
    public interface TestInterface : IComponent { }

    public class ComponentManagerTests
    {
        private ComponentTypeManager sut;

        public ComponentManagerTests()
        {
            sut = new ComponentTypeManager();
        }

        [Fact]
        public void TestTypeNotRegistered()
        {
            ComponentType retVal = sut.GetType<TestRegularComponent>();
            int idx = sut.GetBitIndex<TestRegularComponent>();

            Assert.Null(retVal);
            Assert.Equal(int.MinValue, idx);
        }

        [Fact]
        public void TestNotComponentRegister()
        {
            sut.RegisterType(typeof(TestNotComponent));

            ComponentType retVal = sut.GetType(typeof(TestNotComponent));

            Assert.Null(retVal);
        }

        [Fact]
        public void TestInterfacetRegister()
        {
            sut.RegisterType(typeof(TestInterface));

            ComponentType retVal = sut.GetType(typeof(TestInterface));

            Assert.Null(retVal);
        }

        [Fact]
        public void TestRegularRegister()
        {
            sut.RegisterType(typeof(TestRegularComponent));

            ComponentType retVal = sut.GetType<TestRegularComponent>();
            int idx = sut.GetBitIndex<TestRegularComponent>();

            Assert.NotNull(retVal);
            Assert.Equal(0, idx);
        }

        [Fact]
        public void TestInitialize()
        {
            sut.Initialize(new List<Type>() { typeof(TestRegularComponent) });

            ComponentType retVal = sut.GetType<TestRegularComponent>();
            int idx = sut.GetBitIndex<TestRegularComponent>();

            Assert.NotNull(retVal);
            Assert.Equal(0, idx);
        }

        [Fact]
        public void TestBlankInitialize()
        {
            sut.Initialize(new List<Type>() { });

            ComponentType retVal = sut.GetType<TestRegularComponent>();
            int idx = sut.GetBitIndex<TestRegularComponent>();

            Assert.Null(retVal);
            Assert.Equal(int.MinValue, idx);
        }

        [Fact]
        public void TestGetTypeFromIndex()
        {
            sut.Initialize(new List<Type>() { typeof(TestRegularComponent), typeof(TestRegularComponentBeta) });

            var checkA = sut.GetTypesFromBitIndex(0);

            Assert.Contains(typeof(TestRegularComponent), checkA);
            Assert.DoesNotContain(typeof(TestRegularComponentBeta), checkA);
        }

        [Fact]
        public void TestWhenTypeAddedObservable()
        {
            bool didHit = false;
            sut.WhenComponentTypeAdded.Subscribe(obs =>
                {
                    didHit = true;
                });

            sut.RegisterType(typeof(TestRegularComponent));

            Assert.True(didHit);
        }
    }
}
