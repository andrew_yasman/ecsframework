﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class TestNotSystem { }

    public class TestSystemAlpha : ComponentSystem
    {
        public TestSystemAlpha() : 
            base(TypeFilter.All(typeof(TestRegularComponent)))
        {
        }
    }

    public class TestSystemBeta : ComponentSystem
    {
        public TestSystemBeta() :
            base(TypeFilter.None(typeof(TestRegularComponentBeta)))
        {
        }
    }

    public class TestSystemCallback: ComponentSystem
    {
        private Action initCallback;
        private Action updateCallback;

        public TestSystemCallback(Action initCallback, Action updateCallback) :
            base(TypeFilter.Empty())
        {
            this.initCallback = initCallback;
            this.updateCallback = updateCallback;
        }

        public override void Update(List<EntityComponents> entities, long delta)
        {
            updateCallback.Invoke();
        }

        public override void OnInitialized()
        {
            initCallback.Invoke();
        }
    }

    public class TestSystemEntityRegistration : ComponentSystem
    {
        private Action addedCallback;
        private Action removedCallback;
        private Action updateCallback;

        public TestSystemEntityRegistration(Action addedCallback, Action removedCallback, Action updateCallback) :
            base(TypeFilter.All(typeof(TestRegularComponent)).GetNone(typeof(TestRegularComponentBeta)))
        {
            this.addedCallback = addedCallback;
            this.removedCallback = removedCallback;
            this.updateCallback = updateCallback;
        }

        public override void OnAdded(Entity entity)
        {
            addedCallback.Invoke();
        }

        public override void OnRemoved(Entity entity)
        {
            removedCallback.Invoke();
        }

        public override void Update(List<EntityComponents> entities, long delta)
        {
            updateCallback.Invoke();
        }
    }


    public class SystemManagerTests
    {
        private SystemManager sut;
        private EntityManager em;

        public SystemManagerTests()
        {
            var s = new State(new NullScheduler());
            var cm = new ComponentTypeManager();
            em = new EntityManager(cm, null, s);
            sut = new SystemManager(null, em, cm, s, new NullScheduler());
        }

        [Fact]
        public void TestRegisterCreatedSystem()
        {
            var newSystem = new TestSystemAlpha();
            sut.RegisterSystem(newSystem);
            var retVal = sut.GetSystem(0);

            Assert.NotNull(retVal);
            Assert.IsAssignableFrom<ComponentSystem>(retVal.System);
        }

        [Fact]
        public void TestRegisterTypedSystem()
        {
            bool hitStream = false;
            sut.WhenSystemAdded.Subscribe(obs =>
            {
                hitStream = true;
            });
            sut.RegisterSystem(typeof(TestSystemAlpha));
            var retVal = sut.GetSystem(0);

            Assert.NotNull(retVal);
            Assert.IsAssignableFrom<ComponentSystem>(retVal.System);
            Assert.True(hitStream);
        }

        [Fact]
        public void TestRegisterBadSystem()
        {
            sut.RegisterSystem(typeof(TestNotSystem));
            var retVal = sut.GetSystem(0);

            Assert.Null(retVal);
        }

        [Fact]
        public void TestRegisterCreatedSystemList()
        {
            var newSystem = new TestSystemAlpha();
            var newSystem2 = new TestSystemBeta();
            List<ComponentSystem> sysList = new List<ComponentSystem>() { newSystem, newSystem2 };
            sut.RegisterSystems(sysList);
            var retVal = sut.GetSystem(0);
            var retVal2 = sut.GetSystem(1);

            Assert.NotNull(retVal);
            Assert.NotNull(retVal2);
            Assert.IsAssignableFrom<ComponentSystem>(retVal.System);
            Assert.IsAssignableFrom<ComponentSystem>(retVal2.System);
            Assert.IsType<TestSystemAlpha>(retVal.System);
            Assert.IsType<TestSystemBeta>(retVal2.System);
            Assert.Contains(0, retVal.BitIdentification.GetBitIndexes());
            Assert.Contains(1, retVal2.BitIdentification.GetBitIndexes());
        }

        [Fact]
        public void TestGetSystemList()
        {
            var newSystem = new TestSystemAlpha();
            var newSystem2 = new TestSystemBeta();
            List<ComponentSystem> sysList = new List<ComponentSystem>() { newSystem, newSystem2 };
            sut.RegisterSystems(sysList);
            var retVal = sut.GetSystemsFromBitIndex(0, 1).ToList();
            
            Assert.IsType<TestSystemAlpha>(retVal[0]);
            Assert.IsType<TestSystemBeta>(retVal[1]);
        }

        [Fact]
        public void TestLoopCalls()
        {
            bool hitInit = false;
            bool hitUpdate = false;
            Action onInit = () =>
            {
                hitInit = true;
            };
            Action onUpdate = () =>
            {
                hitUpdate = true;
            };
            var newSystem = new TestSystemCallback(onInit, onUpdate);

            sut.RegisterSystem(newSystem);
            sut.OnInitialized();
            sut.Update(1000);
            Assert.True(hitInit);
            Assert.True(hitUpdate);
        }

        [Fact]
        public void TestEntityRegistrations()
        {
            int counter = 0;
            Action onAdded = () =>
            {
                counter++;
            };
            Action onRemoved = () =>
            {
                counter--;
            };
            var newSystem = new TestSystemEntityRegistration(onAdded, onRemoved, null);

            sut.RegisterSystem(newSystem);
            sut.OnInitialized();

            Entity e1 = em.CreateEntity();

            Assert.Equal(0, counter);

            e1.AddComponent<TestRegularComponent>();
            Assert.Equal(1, counter);

            e1.AddComponent<TestRegularComponentBeta>();
            Assert.Equal(0, counter);

            e1.RemoveComponent<TestRegularComponentBeta>();
            Assert.Equal(1, counter);

            e1.RemoveComponent<TestRegularComponent>();
            Assert.Equal(0, counter);
        }

        [Fact]
        public void TestEntityRefreshScheduled()
        {
            var s = new State(new NullScheduler());
            var cm = new ComponentTypeManager();
            var em = new EntityManager(cm, null, s);
            var sch = new DefaultScheduler();
            var sut = new SystemManager(null, em, cm, s, sch);

            bool added = false;
            bool removed = false;
            bool update = false;
            Action onAdded = () =>
            {
                added = true;
            };
            Action onRemoved = () =>
            {
                removed = true;
            };
            Action onUpdate = () =>
            {
                update = true;
            };
            var newSystem = new TestSystemEntityRegistration(onAdded, onRemoved, onUpdate);

            sut.RegisterSystem(newSystem);
            sut.OnInitialized();

            sch.InUpdate = true;

            Entity e1 = em.CreateEntity();
            e1.AddComponent<TestRegularComponent>();
            e1.AddComponent<TestRegularComponentBeta>();

            sch.InUpdate = false;
            sch.Synchronize();
        }
    }
}
