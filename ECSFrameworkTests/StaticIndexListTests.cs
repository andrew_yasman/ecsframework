﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class StaticIndexListTests
    {
        [Fact]
        public void TestStaticIndexList()
        {
            StaticIndexList<IComponent> sl = new StaticIndexList<IComponent>();

            int c1 = sl.Insert(new TestRegularComponent());
            int c2 = sl.Insert(5, new TestRegularComponentBeta());

            sl.At(0);
            sl.RemoveAt(0);
            sl.At(0);
            sl.Insert(new TestRegularComponent());
        }

        [Fact]
        public void TestRegularInsert()
        {
            StaticIndexList<int> sl = new StaticIndexList<int>();

            Assert.Equal(0, sl.Count);
            Assert.Equal(0, sl.Next);

            Assert.Equal(0, sl.Insert(100));
            Assert.Equal(1, sl.Count);
            Assert.Equal(1, sl.Next);

            Assert.Equal(1, sl.Insert(200));
            Assert.Equal(2, sl.Count);
            Assert.Equal(2, sl.Next);

            Assert.Equal(100, sl.At(0));
            Assert.Equal(200, sl.At(1));

            Assert.Equal(200, sl.Find(p => p == 200));
        }

        [Fact]
        public void TestMixedIndexInsert()
        {
            StaticIndexList<int> sl = new StaticIndexList<int>();

            sl.Insert(5, 100);
            sl.Insert(9, 200);
            sl.Insert(500);
            sl.Insert(0, 300);
            sl.Insert(2, 400);
            sl.Insert(600);

            Assert.Equal(100, sl.At(5));
            Assert.Equal(200, sl.At(9));
            Assert.Equal(300, sl.At(0));
            Assert.Equal(400, sl.At(2));
            Assert.Equal(600, sl.At(1));
        }

        [Fact]
        public void TestRemovalInsert()
        {
            StaticIndexList<int> sl = new StaticIndexList<int>();

            sl.Insert(100);
            sl.Insert(200);

            Assert.Equal(100, sl.RemoveAt(0));
            Assert.Equal(2, sl.Count);
            Assert.Equal(0, sl.Next);
            Assert.Equal(0, sl.Insert(300));
        }

        [Fact]
        public void TestOutOfBoundsInsert()
        {
            StaticIndexList<int> sl = new StaticIndexList<int>();

            sl.Insert(100);
            sl.Insert(9, 200);
            sl.Insert(300);

            Assert.Equal(100, sl.At(0));
            Assert.Equal(300, sl.At(1));
            Assert.Equal(200, sl.At(9));
            Assert.Equal(10, sl.Count);
            Assert.Equal(2, sl.Next);
        }

        [Fact]
        public void TestBadListAccess()
        {
            StaticIndexList<IComponent> sl = new StaticIndexList<IComponent>();

            Assert.False(sl.HasAt(-5436));
            Assert.False(sl.HasAt(0));
            Assert.False(sl.HasAt(6544));

            Assert.Null(sl.At(-5436));
            Assert.Null(sl.At(0));
            Assert.Null(sl.At(6544));
        }
    }
}
