﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class TypeFilterTestsComponentA : IComponent {}
    public class TypeFilterTestsComponentB : IComponent { }
    public class TypeFilterTestsComponentC : IComponent { }
    public class TypeFilterTestsComponentD : IComponent { }
    public class TypeFilterTestsComponentE : IComponent { }
    public class TypeFilterTestsComponentF : IComponent { }
    public class TypeFilterTestsComponentG : IComponent { }
    public class TypeFilterTestsComponentH : IComponent { }
    public class TypeFilterTestsComponentI : IComponent { }
    public class TypeFilterTestsComponentJ : IComponent { }

    public class TypeFilterTests
    {
        private int maxComp = 10;

        private BitIdentification biEmpty;
        private BitIdentification biValueA;
        private BitIdentification biValueB;
        private BitIdentification biValueC;

        private ComponentTypeManager componentTypeManager;

        public TypeFilterTests()
        {
            biEmpty = new BitIdentification(maxComp);
            biValueA = new BitIdentification(maxComp, 1, 2);
            biValueB = new BitIdentification(maxComp, 2, 3, 8);
            biValueC = new BitIdentification(maxComp, 6);

            componentTypeManager = new ComponentTypeManager();
            componentTypeManager.RegisterType<TypeFilterTestsComponentA>(); // 0
            componentTypeManager.RegisterType<TypeFilterTestsComponentB>(); // 1
            componentTypeManager.RegisterType<TypeFilterTestsComponentC>(); // 2
            componentTypeManager.RegisterType<TypeFilterTestsComponentD>(); // 3
            componentTypeManager.RegisterType<TypeFilterTestsComponentE>(); // 4
            componentTypeManager.RegisterType<TypeFilterTestsComponentF>(); // 5
            componentTypeManager.RegisterType<TypeFilterTestsComponentG>(); // 6
            componentTypeManager.RegisterType<TypeFilterTestsComponentH>(); // 7
            componentTypeManager.RegisterType<TypeFilterTestsComponentI>(); // 8
            componentTypeManager.RegisterType<TypeFilterTestsComponentJ>(); // 9
        }

        [Fact]
        public void TestEmptyFilter()
        {
            TypeFilter filter = TypeFilter.Empty();
            filter.CalculateFilterID(componentTypeManager);

            Assert.True(filter.AllTypeFilter.Empty);
            Assert.True(filter.AnyTypeFilter.Empty);
            Assert.True(filter.NoneTypeFilter.Empty);

            Assert.True(filter.Matches(biEmpty));
            Assert.True(filter.Matches(biValueA));
            Assert.True(filter.Matches(biValueB));
            Assert.True(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterAll()
        {
            TypeFilter filter = TypeFilter.All(typeof(TypeFilterTestsComponentB), typeof(TypeFilterTestsComponentC));
            filter.CalculateFilterID(componentTypeManager);

            Assert.False(filter.AllTypeFilter.Empty);
            Assert.True(filter.AnyTypeFilter.Empty);
            Assert.True(filter.NoneTypeFilter.Empty);

            Assert.False(filter.Matches(biEmpty));
            Assert.True(filter.Matches(biValueA));
            Assert.False(filter.Matches(biValueB));
            Assert.False(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterAny()
        {
            TypeFilter filter = TypeFilter.Any(typeof(TypeFilterTestsComponentB), typeof(TypeFilterTestsComponentI), typeof(TypeFilterTestsComponentJ));
            filter.CalculateFilterID(componentTypeManager);

            Assert.True(filter.AllTypeFilter.Empty);
            Assert.False(filter.AnyTypeFilter.Empty);
            Assert.True(filter.NoneTypeFilter.Empty);

            Assert.False(filter.Matches(biEmpty));
            Assert.True(filter.Matches(biValueA));
            Assert.True(filter.Matches(biValueB));
            Assert.False(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterNone()
        {
            TypeFilter filter = TypeFilter.None(typeof(TypeFilterTestsComponentB), typeof(TypeFilterTestsComponentI), typeof(TypeFilterTestsComponentJ));
            filter.CalculateFilterID(componentTypeManager);

            Assert.True(filter.AllTypeFilter.Empty);
            Assert.True(filter.AnyTypeFilter.Empty);
            Assert.False(filter.NoneTypeFilter.Empty);

            Assert.True(filter.Matches(biEmpty));
            Assert.False(filter.Matches(biValueA));
            Assert.False(filter.Matches(biValueB));
            Assert.True(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterMixedHitA()
        {
            TypeFilter filter = TypeFilter.All(typeof(TypeFilterTestsComponentC))
                                          .GetAny(typeof(TypeFilterTestsComponentI), typeof(TypeFilterTestsComponentJ))
                                          .GetNone(typeof(TypeFilterTestsComponentB));
            filter.CalculateFilterID(componentTypeManager);

            Assert.False(filter.AllTypeFilter.Empty);
            Assert.False(filter.AnyTypeFilter.Empty);
            Assert.False(filter.NoneTypeFilter.Empty);

            Assert.False(filter.Matches(biEmpty));
            Assert.False(filter.Matches(biValueA));
            Assert.True(filter.Matches(biValueB));
            Assert.False(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterMixedHitB()
        {
            TypeFilter filter = TypeFilter.Any(typeof(TypeFilterTestsComponentC), typeof(TypeFilterTestsComponentD), typeof(TypeFilterTestsComponentG))
                                          .GetNone(typeof(TypeFilterTestsComponentB));
            filter.CalculateFilterID(componentTypeManager);

            Assert.True(filter.AllTypeFilter.Empty);
            Assert.False(filter.AnyTypeFilter.Empty);
            Assert.False(filter.NoneTypeFilter.Empty);

            Assert.False(filter.Matches(biEmpty));
            Assert.False(filter.Matches(biValueA));
            Assert.True(filter.Matches(biValueB));
            Assert.True(filter.Matches(biValueC));
        }

        [Fact]
        public void TestFilterAutoRegister()
        {
            TypeFilter filter = TypeFilter.All(typeof(TypeFilterTestsComponentB));
            filter.CalculateFilterID(new ComponentTypeManager());

            Assert.False(filter.AllTypeFilter.Empty);
        }
    }
}
