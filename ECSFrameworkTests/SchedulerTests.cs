﻿using AYLib.ECSFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AYLib.ECSFrameworkTests
{
    public class SchedulerTests
    {
        [Fact]
        public void TestNullScheduler()
        {
            NullScheduler nullSut = new NullScheduler();

            Assert.False(nullSut.InUpdate);

            nullSut.InUpdate = true;
            Assert.True(nullSut.InUpdate);

            bool hitHere = false;
            Action setAction = () => hitHere = true;
            nullSut.Add(setAction);

            Assert.True(hitHere);
        }

        [Fact]
        public void TestDefaultScheduler()
        {
            bool hitHere = false;
            DefaultScheduler defaultSut = new DefaultScheduler();
            Action setAction = () => hitHere = true;

            Assert.False(defaultSut.InUpdate);
            
            defaultSut.Add(setAction);
            Assert.True(hitHere);

            hitHere = false;
            defaultSut.InUpdate = true;
            Assert.True(defaultSut.InUpdate);
            defaultSut.Add(setAction);
            Assert.False(hitHere);
            defaultSut.Synchronize();
            Assert.True(hitHere);
        }
    }
}
