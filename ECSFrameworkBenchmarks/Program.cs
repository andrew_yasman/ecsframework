﻿using AYLib.ECSFramework;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Diagnosers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

using DefaultEntity = DefaultEcs.Entity;
using DefaultEntitySet = DefaultEcs.EntitySet;
using DefaultWorld = DefaultEcs.World;
using DefaultEcs.System;
using DefaultEcs;
using DefaultEcs.Threading;

namespace ECSFrameworkBenchmarks
{
    public class HashCheckTester
    {
        private int numSubset = 10;
        private int numFull = 60;
        private int count = 30000;

        private HashSet<int> subsetHashset;
        private List<HashSet<int>> fullHashSet;
        private List<bool[]> fullBinaryArray;
        private List<int> fullBitCheck;
        private List<BitArray> fullBitArray;
        private List<BitIdentification> fullBitID;
        private int masterBitCheck = 0;
        private BitIdentification masterBitCheckID;

        public HashCheckTester()
        {
            Random rng = new Random();
            masterBitCheckID = new BitIdentification(numFull);

            subsetHashset = new HashSet<int>();
            for (int i=0; i< numSubset; i++)
            {
                int nextNum = rng.Next(1, numFull);
                if (!subsetHashset.Contains(nextNum))
                    subsetHashset.Add(nextNum);
                else
                    i--;
            }

            fullHashSet = new List<HashSet<int>>();
            fullBinaryArray = new List<bool[]>();
            fullBitCheck = new List<int>();
            fullBitArray = new List<BitArray>();
            fullBitID = new List<BitIdentification>();

            for (int i = 0; i < count; i++)
            {
                bool[] runList = new bool[numFull+1];
                HashSet<int> runHash = new HashSet<int>();
                int bitCheck = 0;
                BitIdentification tempBitID = new BitIdentification(numFull);

                for (int j = 1; j <= numFull; j++)
                {
                    double percent = rng.NextDouble();
                    if (percent >= 0.8)
                    {
                        runHash.Add(j);
                        runList[j] = true;

                        int newBit = 0;
                        newBit |= 1 << j;

                        bitCheck |= newBit;

                        tempBitID.SetBit(j);
                    }
                    else
                    {
                        runList[j] = false;
                    }
                }

                BitArray bitAry = new BitArray(runList);

                fullHashSet.Add(runHash);
                fullBinaryArray.Add(runList);
                fullBitCheck.Add(bitCheck);
                fullBitArray.Add(bitAry);
                fullBitID.Add(tempBitID);
            }

            int newBitbbb = 0;
            foreach (var check in subsetHashset)
            {
                newBitbbb |= 1 << check;
                masterBitCheck |= newBitbbb;

                masterBitCheckID.SetBit(check);
            }
        }

        //[Benchmark]
        public bool Test1()
        {
            bool retVal = true;
            foreach (var chunk in fullHashSet)
            {
                retVal = subsetHashset.IsSubsetOf(chunk);
            }
            return retVal;
        }

        [Benchmark]
        public bool Test2()
        {
            bool retVal = true;
            foreach (var chunk in fullHashSet)
            {
                bool found = true;
                foreach (var check in subsetHashset)
                {
                    if (!chunk.Contains(check))
                    {
                        found = false;
                        break;
                    }
                }
                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test3()
        {
            bool retVal = true;
            foreach (var chunk in fullBinaryArray)
            {
                bool found = true;
                foreach (var check in subsetHashset)
                {
                    if (chunk[check] == false)
                    {
                        found = false;
                        break;
                    }
                }
                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test4()
        {
            bool retVal = true;
            foreach (var chunk in fullBitCheck)
            {
                bool found = true;
                foreach (var check in subsetHashset)
                {
                    int newBit = 0;
                    newBit |= 1 << check;

                    if ((chunk & newBit) == 0)
                    {
                        found = false;
                        break;
                    }
                }
                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test5()
        {
            bool retVal = true;

            foreach (var chunk in fullBitCheck)
            {
                bool found = true;

                if ((chunk & masterBitCheck) != masterBitCheck)
                    found = false;

                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test7()
        {
            bool retVal = true;

            foreach (var chunk in fullBitID)
            {
                bool found = true;

                if (!masterBitCheckID.All(chunk))
                    found = false;

                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test8()
        {
            bool retVal = true;

            for(int i=0; i<fullBitID.Count; i++)
            {
                bool found = true;

                if (!masterBitCheckID.All(fullBitID[i]))
                    found = false;

                retVal = found;
            }
            return retVal;
        }

        //[Benchmark]
        public bool Test6()
        {
            bool retVal = true;
            BitArray bitAry = new BitArray(numFull+1);
            foreach (var check in subsetHashset)
            {
                bitAry.Set(check, true);
            }

            foreach (var chunk in fullBitArray)
            {
                bool found = true;
                //foreach (var check in subsetHashset)
                //{
                //    int newBit = 0;
                //    newBit |= 1 << check;

                    

                    var ttt = bitAry.And(chunk);
                    //if ((chunk & newBit) == 0)
                    //{
                    //    found = false;
                    //    break;
                    //}
                //}
                retVal = found;
            }
            return retVal;
        }

        [Benchmark]
        public bool Test10()
        {
            bool retVal = true;

            for (int i = 0; i < fullBitID.Count; i++)
            {
                bool found = true;

                if (!masterBitCheckID.All(fullBitID[i]))
                    found = false;

                retVal = found;
            }
            return retVal;
        }
    }

    [MemoryDiagnoser]
    public class ListTester11111
    {
        private int maxSize = 100000000;
        private List<string> randoList = new List<string>();
        private string[] randoArray;
        private Memory<string> componentMemory;

        public ListTester11111()
        {
            randoArray = new string[maxSize];

            for (int i = 0; i < maxSize; i++)
            {
                randoList.Add("fdsfdsfdsfsdfds");
                randoArray[i] = "fdsfdsfdsfsdfds";
            }

            componentMemory = new Memory<string>(randoArray);
        }

        [Benchmark]
        public string Test1()
        {
            string ret = string.Empty;
            for (int i = 0; i < randoList.Count; i++)
            {
                ret = randoList[i];
            }
            return ret;
        }

        [Benchmark]
        public string Test2()
        {
            string ret = string.Empty;
            for (int i = 0; i < randoArray.Length; i++)
            {
                ret = randoArray[i];
            }
            return ret;
        }

        [Benchmark]
        public string Test3()
        {
            var sp = componentMemory.Span;
            string ret = string.Empty;
            for (int i = 0; i < sp.Length; i++)
            {
                ret  = sp[i];
            }
            return ret;
        }
    }

    [MemoryDiagnoser]
    public class RandomAccessTester
    {
        private List<string> randoList = new List<string>();
        private Dictionary<int, string> randoDict = new Dictionary<int, string>();

        public RandomAccessTester()
        {
            for(int i=0; i<10000; i++)
            {
                randoList.Add("fdsfdsfdsfsdfds");
                randoDict.Add(i, "fdsfdsfdsfsdfds");
            }
        }

        [Benchmark]
        public string Test1()
        {
            string ret = string.Empty;
            Random rng = new Random();
            for(int i=0; i<1000000; i++)
            {
                ret = randoList[rng.Next(0, 10000)];
            }
            return ret;
        }

        [Benchmark]
        public string Test2()
        {
            string ret = string.Empty;
            Random rng = new Random();
            for (int i = 0; i < 1000000; i++)
            {
                ret = randoDict[rng.Next(0, 10000)];
            }
            return ret;
        }
    }

    public class TestRegularComponent : IComponent
    {
        int i;
        int j;

        public TestRegularComponent()
        {
            i = 54235;
            j = 354;
        }
    }

    [MemoryDiagnoser]
    public class ListTester
    {
        StaticIndexList<Dictionary<int, IComponent>> componentData = new StaticIndexList<Dictionary<int, IComponent>>();
        StaticIndexList<StaticIndexList<IComponent>> componentData2 = new StaticIndexList<StaticIndexList<IComponent>>();

        public ListTester()
        {
            Random rng = new Random();
            for (int i = 0; i < 1000; i++)
            {
                var newDict = new Dictionary<int, IComponent>();
                var newList = new StaticIndexList<IComponent>();

                for (int j = 0; j < 500; j++)
                {
                    var next = rng.Next(j * 10, j * 10 + 10);
                    var data = new TestRegularComponent();

                    newDict.Add(next, data);
                    newList.Insert(next, data);
                }

                componentData.Insert(newDict);
                componentData2.Insert(newList);
            }
        }

        [Benchmark]
        public IComponent Test1()
        {
            IComponent retVal = null;
            Random rng = new Random();
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    var next = rng.Next(0, 5000);
                    if (componentData.At(i).ContainsKey(next))
                    {
                        retVal = componentData.At(i)[next];
                    }
                }
            }
            return retVal;
        }

        [Benchmark]
        public IComponent Test2()
        {
            IComponent retVal = null;
            Random rng = new Random();
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    var next = rng.Next(0, 5000);
                    if (componentData2.At(i).HasAt(next))
                    {
                        retVal = componentData2.At(i).At(next);
                    }
                }
            }
            return retVal;
        }
    }

    [MemoryDiagnoser]
    public class TestEngineBenchmarks
    {
        private const long Time = (long)(1f / 60f);

        private struct Speed : IComponent
        {
            public float X;
            public float Y;
        }

        private struct Position : IComponent
        {
            public float X;
            public float Y;
        }

        private struct DefaultSpeed
        {
            public float X;
            public float Y;
        }

        private struct DefaultPosition
        {
            public float X;
            public float Y;
        }

        private sealed class DefaultEcsComponentSystem : AEntitySystem<float>
        {
            private readonly DefaultWorld _world;

            public DefaultEcsComponentSystem(DefaultWorld world, IParallelRunner runner)
                : base(world.GetEntities().With<DefaultSpeed>().With<DefaultPosition>().AsSet(), runner)
            {
                _world = world;
            }

            protected override void Update(float state, ReadOnlySpan<DefaultEntity> entities)
            {
                Components<DefaultSpeed> speeds = _world.GetComponents<DefaultSpeed>();
                Components<DefaultPosition> positions = _world.GetComponents<DefaultPosition>();

                foreach (ref readonly DefaultEntity entity in entities)
                {
                    //DefaultSpeed speed = speeds[entity];
                    //ref DefaultPosition position = ref positions[entity];

                    //position.X += speed.X * state;
                    //position.Y += speed.Y * state;

                    entity.Set<Speed>();
                }
            }
        }

        private sealed class DefaultEcsComponentSystem2 : AEntitySystem<float>
        {
            private readonly DefaultWorld _world;

            public DefaultEcsComponentSystem2(DefaultWorld world, IParallelRunner runner)
                : base(world.GetEntities().With<Speed>().AsSet(), runner)
            {
                _world = world;
            }

            protected override void Update(float state, ReadOnlySpan<DefaultEntity> entities)
            {

            }
        }

        public class MyTestSystem : ComponentSystem
        {
            public MyTestSystem() :
                base(TypeFilter.All(typeof(Speed), typeof(Position)))
            {

            }

            public override void Update(List<EntityComponents> entities, long delta)
            {
                if (entities != null)
                {
                    for(int i=0; i< entities.Count; i++)
                    {
                        var e = entities[i];
                        var ecl = entities[i].ComponentsList;
                        //var s = e.ComponentsList[0];
                        //var p = e.ComponentsList[1];
                        //p.X += s.X * delta;
                        //p.Y += s.Y * delta;
                    }
                }
            }
        }

        private EntityWorld myWorldType;
        private MyTestSystem testSystem;
        private DefaultEcsComponentSystem _defaultComponentSystem;
        private DefaultEcsComponentSystem2 _defaultComponentSystem2;
        private DefaultWorld _defaultWorld;

        [GlobalSetup]
        public void Setup()
        {
            _defaultWorld = new DefaultWorld(100000);
            _defaultComponentSystem = new DefaultEcsComponentSystem(_defaultWorld, null);
            _defaultComponentSystem2 = new DefaultEcsComponentSystem2(_defaultWorld, null);

            myWorldType = new EntityWorld();
            testSystem = new MyTestSystem();
            myWorldType.RegisterSystem(testSystem);
            myWorldType.Initialize();

            for (int i = 0; i < 100000; ++i)
            {
                AYLib.ECSFramework.Entity entity = myWorldType.CreateEntity();
                entity.AddComponent<Position>();
                entity.AddComponent(new Speed { X = 42, Y = 42 });

                DefaultEntity defaultEntity = _defaultWorld.CreateEntity();
                defaultEntity.Set<DefaultPosition>();
                defaultEntity.Set(new DefaultSpeed { X = 42, Y = 42 });
            }
        }

        [GlobalCleanup]
        public void Cleanup()
        {
            myWorldType.Dispose();
        }

        [Benchmark]
        public void My_ComponentSystem() => testSystem.Update(Time);

        [Benchmark]
        public void DefaultEcs_ComponentSystem()
        {
            _defaultComponentSystem.Update(Time);
            _defaultComponentSystem2.Update(Time);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // rrr = new ListTester11111();
            //rrr.Test3();
            //ListTester c = new ListTester();
            //c.Test2();

            //System.Diagnostics.Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
            //TestEngineBenchmarks x = new TestEngineBenchmarks();
            //x.Setup();
            //x.Setup();
            //x.My_ComponentSystem();
            //sw1.Stop();

            //System.Diagnostics.Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
            //x.DefaultEcs_ComponentSystem();
            //x.My_ComponentSystem();
            //sw1.Stop();

            var summary = BenchmarkRunner.Run<HashCheckTester>();
        }
    }
}
