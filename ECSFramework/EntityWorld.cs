﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("ECSFrameworkTests")]
[assembly: InternalsVisibleTo("ECSFrameworkBenchmarks")]
namespace AYLib.ECSFramework
{
    public class EntityWorld : IDisposable
    {
        private ComponentTypeManager componentTypeManager;
        private SystemManager systemManager;
        private EntityManager entityManager;
        private IScheduler scheduler;

        private State internalState;

        public EntityWorld(IScheduler overrideSceheduler = null, Settings overrideSettings = null)
        {
            scheduler = overrideSceheduler ?? new DefaultScheduler();
            internalState = new State(scheduler, overrideSettings);
            componentTypeManager = new ComponentTypeManager();
            entityManager = new EntityManager(componentTypeManager, scheduler, internalState);
            systemManager = new SystemManager(this, entityManager, componentTypeManager, internalState, scheduler);
        }

        public void Register(List<Type> componentTypes, List<ComponentSystem> componentSystems)
        {
            componentTypeManager.Initialize(componentTypes);
            systemManager.RegisterSystems(componentSystems);
        }

        public void RegisterComponentType(Type componentType)
        {
            componentTypeManager.RegisterType(componentType);
        }

        public void RegisterComponentType<T>() where T : IComponent
        {
             componentTypeManager.RegisterType(typeof(T));
        }

        public void RegisterSystem(ComponentSystem componentSystem)
        {
            systemManager.RegisterSystem(componentSystem);
        }

        public void RegisterSystem(Type componentSystem)
        {
            systemManager.RegisterSystem(componentSystem);
        }

        public void RegisterSystem<T>() where T : ComponentSystem
        {
            systemManager.RegisterSystem(typeof(T));
        }

        public Entity CreateEntity()
        {
            return entityManager.CreateEntity();
        }

        public void DeleteEntity(Entity entity)
        {
            entityManager.Remove(entity);
        }

        public Entity GetEntity(int entityID)
        {
            return entityManager.GetEntity(entityID);
        }

        public void Initialize()
        {

        }

        public void Update(long delta)
        {
            if (internalState.SyncBeforeUpdate)
                scheduler.Synchronize();

            scheduler.InUpdate = true;
            systemManager.Update(delta);
            scheduler.InUpdate = false;

            if (internalState.SyncAfterUpdate)
                scheduler.Synchronize();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    componentTypeManager.Dispose();
                    systemManager.Dispose();
                    entityManager.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
