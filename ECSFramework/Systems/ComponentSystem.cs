﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace AYLib.ECSFramework
{
    public class EntityComponents
    {
        public Entity Entity { get; private set; }
        public Dictionary<Type, IComponent> Components { get; private set; }
        public List<IComponent> ComponentsList { get; private set; }

        public EntityComponents(Entity entity, Dictionary<Type, IComponent> components)
        {
            Entity = entity;
            Components = components;
            ComponentsList = components.Values.ToList();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IComponent Component<T>()
        {
            return Components[typeof(T)];
        }
    }

    public class ComponentSystem : IDisposable
    {
        public EntityWorld EntityWorld { get; internal set; }
        public IScheduler Scheduler { get; internal set; }

        private TypeFilter Filter { get; set; }

        private bool refreshScheduled = false;

        private Dictionary<int, Entity> assignedEntities = new Dictionary<int, Entity>();
        private List<EntityComponents> activeEntities = new List<EntityComponents>();

        public ComponentSystem(TypeFilter filter)
        {
            Filter = filter;
        }

        internal void Initialize(ComponentTypeManager componentTypeManager, EntityManager entityManager, EntityWorld entityWorld, IScheduler scheduler)
        {
            EntityWorld = entityWorld;
            Scheduler = scheduler;
            Filter.CalculateFilterID(componentTypeManager);

            entityManager.WhenComponentAdded.Subscribe(changeData =>
            {
                if (!assignedEntities.ContainsKey(changeData.Entity.ID) && Filter.Matches(changeData.Entity.AssignedComponents))
                {
                    AddEntity(changeData.Entity);
                }
                else if(assignedEntities.ContainsKey(changeData.Entity.ID) && !Filter.Matches(changeData.Entity.AssignedComponents))
                {
                    RemoveEntity(changeData.Entity);
                }
            });

            entityManager.WhenComponentRemoved.Subscribe(changeData =>
            {
                if (assignedEntities.ContainsKey(changeData.Entity.ID) && !Filter.Matches(changeData.Entity.AssignedComponents))
                {
                    RemoveEntity(changeData.Entity);
                }
                else if (!assignedEntities.ContainsKey(changeData.Entity.ID) && Filter.Matches(changeData.Entity.AssignedComponents))
                {
                    AddEntity(changeData.Entity);
                }
            });

            entityManager.WhenEntityAdded.Subscribe(entity =>
            {
                if (!assignedEntities.ContainsKey(entity.ID) && Filter.Matches(entity.AssignedComponents))
                {
                    AddEntity(entity);
                }
            });

            entityManager.WhenEntityRemoved.Subscribe(entity =>
            {
                if (assignedEntities.ContainsKey(entity.ID))
                {
                    RemoveEntity(entity);
                }
            });

            componentTypeManager.WhenComponentTypeAdded.Subscribe(ct =>
            {
                Filter.Resize(componentTypeManager.ComponentCount);
            });
        }

        private void RemoveEntity(Entity entity)
        {
            assignedEntities.Remove(entity.ID);
            ScheduleRefresh();
            OnRemoved(entity);
        }

        private void AddEntity(Entity entity)
        {
            assignedEntities.Add(entity.ID, entity);
            ScheduleRefresh();
            OnAdded(entity);
        }

        private void ScheduleRefresh()
        {
            if (!refreshScheduled)
            {
                refreshScheduled = true;
                Scheduler.Add(() =>
                {
                    ResetActiveEntities();
                    refreshScheduled = false;
                });
            }
        }

        private void ResetActiveEntities()
        {
            activeEntities.Clear();
            foreach (var entity in assignedEntities)
            {
                activeEntities.Add(new EntityComponents(entity.Value, entity.Value.GetComponents()));
            }
        }

        public virtual void OnAdded(Entity entity)
        {

        }

        public virtual void OnRemoved(Entity entity)
        {

        }

        public virtual void OnInitialized()
        {

        }

        public virtual void OnDestroy()
        {

        }

        internal void Update(long delta)
        {
            this.Update(activeEntities, delta);
        }

        public virtual void Update(List<EntityComponents> entities, long delta)
        {

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    OnDestroy();

                    activeEntities.Clear();
                    activeEntities = null;

                    Filter.Dispose();
                    Filter = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
