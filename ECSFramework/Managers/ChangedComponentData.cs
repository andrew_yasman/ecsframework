﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class ChangedComponentData
    {
        public Entity Entity { get; private set; }
        public IComponent Component { get; private set; }

        internal ChangedComponentData(Entity entity, IComponent component)
        {
            Entity = entity;
            Component = component;
        }
    }
}
