﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class SystemType : IDisposable
    {
        private ComponentSystem system;
        private int bitIndex;
        private BitIdentification bitID;

        public SystemType(int bit, ComponentSystem system)
        {
            bitIndex = bit;
            this.system = system;
        }

        public ComponentSystem System => system;
        public int BitIndex => bitIndex;
        public BitIdentification BitIdentification => bitID;

        public void CalculateBitID(int maxSystems)
        {
            bitID = new BitIdentification(maxSystems, BitIndex);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    system.Dispose();
                    bitID.Dispose();

                    system = null;
                    bitID = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
