﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class ComponentTypeManager : IDisposable
    {
        private Subject<ComponentType> componentTypeAdded = new Subject<ComponentType>();
        private Subject<ComponentType> componentTypeRemoved = new Subject<ComponentType>();

        public IObservable<ComponentType> WhenComponentTypeAdded => componentTypeAdded.AsObservable().Publish().RefCount();
        public IObservable<ComponentType> WhenComponentTypeRemoved => componentTypeRemoved.AsObservable().Publish().RefCount();

        private int nextIndex = 0;
        private List<ComponentType> componentTypeData = new List<ComponentType>();

        internal int ComponentCount => componentTypeData.Count;

        public ComponentTypeManager()
        {

        }

        public void Initialize(List<Type> types)
        {
            foreach (var newType in types)
            {
                RegisterTypeInternal(newType);
            }
            CalculateBitIDs();
        }

        public void RegisterType(Type newType)
        {
            RegisterTypeInternal(newType);
            CalculateBitIDs();
        }

        public void RegisterType<T>() where T : IComponent
        {
            RegisterTypeInternal(typeof(T));
            CalculateBitIDs();
        }

        private void RegisterTypeInternal(Type newType)
        {
            if (typeof(IComponent).IsAssignableFrom(newType))
            {
                if (newType.IsInterface)
                    return;

                ComponentType newComponentType = new ComponentType(nextIndex++, newType);
                componentTypeData.Insert(newComponentType.BitIndex, newComponentType);

                componentTypeAdded?.OnNext(newComponentType);
            }
        }

        private void CalculateBitIDs()
        {
            int componentCount = componentTypeData.Count;
            foreach (var ct in componentTypeData)
            {
                ct.CalculateBitID(componentCount);
            }
        }

        public ComponentType GetType(int idx)
        {
            if (componentTypeData.Count > idx)
                return componentTypeData[idx];
            return null;
        }

        public int GetBitIndex<T>() where T : IComponent
        {
            return GetType<T>()?.BitIndex ?? int.MinValue;
        }

        public int GetBitIndex(Type toFind)
        {
            return GetType(toFind)?.BitIndex ?? int.MinValue;
        }

        public ComponentType GetType<T>() where T : IComponent
        {
            return GetType(typeof(T));
        }

        public ComponentType GetType(Type toFind)
        {
            foreach (var ct in componentTypeData)
            {
                if (ct.DataType == toFind)
                    return ct;
            }
            return null;
        }

        public IEnumerable<Type> GetTypesFromBitIndex(params int[] indexes)
        {
            foreach (var idx in indexes)
            {
                if (componentTypeData.Count > idx)
                {
                    yield return componentTypeData[idx].DataType;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    componentTypeAdded.Dispose();
                    componentTypeRemoved.Dispose();

                    componentTypeAdded = null;
                    componentTypeRemoved = null;

                    componentTypeData.ForEach(ctd => ctd.Dispose());
                    componentTypeData.Clear();
                    componentTypeData = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
