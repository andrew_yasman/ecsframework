﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class SystemManager : IDisposable
    {
        private Subject<ComponentSystem> systemAdded = new Subject<ComponentSystem>();
        private Subject<ComponentSystem> systemRemoved = new Subject<ComponentSystem>();

        public IObservable<ComponentSystem> WhenSystemAdded => systemAdded.AsObservable().Publish().RefCount();
        public IObservable<ComponentSystem> WhenSystemRemoved => systemRemoved.AsObservable().Publish().RefCount();

        private readonly EntityWorld entityWorld;
        private readonly EntityManager entityManager;
        private readonly ComponentTypeManager componentTypeManager;
        private readonly IScheduler scheduler;
        private readonly State internalState;

        private int nextIndex = 0;
        private List<SystemType> systemTypes = new List<SystemType>();

        public SystemManager(EntityWorld entityWorld, EntityManager entityManager, ComponentTypeManager componentTypeManager, State internalState, IScheduler scheduler)
        {
            this.internalState = internalState;
            this.entityWorld = entityWorld;
            this.entityManager = entityManager;
            this.componentTypeManager = componentTypeManager;
            this.scheduler = scheduler;
        }

        public void RegisterSystem(ComponentSystem newSystem)
        {
            RegisterSystemInternal(newSystem);
        }

        public void RegisterSystems(List<ComponentSystem> newSystems)
        {
            newSystems.ForEach(s => RegisterSystemInternal(s));
        }

        public void RegisterSystem(Type newType)
        {
            if (typeof(ComponentSystem).IsAssignableFrom(newType))
            {
                if (newType.IsInterface)
                    return;

                ComponentSystem newSystem = Activator.CreateInstance(newType) as ComponentSystem;

                RegisterSystemInternal(newSystem);
            }
        }

        private void RegisterSystemInternal(ComponentSystem newSystem)
        {
            SystemType newSystemType = new SystemType(nextIndex++, newSystem);
            systemTypes.Insert(newSystemType.BitIndex, newSystemType);

            newSystem.Initialize(componentTypeManager, entityManager, entityWorld, scheduler);

            CalculateBitIDs();

            systemAdded?.OnNext(newSystem);
        }

        private void CalculateBitIDs()
        {
            int systemCount = systemTypes.Count;
            foreach (var st in systemTypes)
            {
                st.CalculateBitID(systemCount);
            }
        }

        public SystemType GetSystem(int idx)
        {
            if (systemTypes.Count > idx)
                return systemTypes[idx];
            return null;
        }

        public IEnumerable<ComponentSystem> GetSystemsFromBitIndex(params int[] indexes)
        {
            foreach (var idx in indexes)
            {
                if (systemTypes.Count > idx)
                {
                    yield return systemTypes[idx].System;
                }
            }
        }

        /// <summary>
        /// Primary entrance for the update loop. This will filter all the components that a system will require and
        /// send them in.
        /// </summary>
        public void Update(long delta)
        {
            for(int i=0; i< systemTypes.Count; i++)
            {
                systemTypes[i].System.Update(delta);
            }
            //foreach (var system in systemTypes.Select(p => p.System))
            //{
            //    system.Update(delta);
            //}
        }

        public void OnInitialized()
        {
            foreach (var system in systemTypes.Select(p => p.System))
            {
                system.OnInitialized();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    systemTypes.ForEach(st => st.Dispose());
                    systemTypes.Clear();
                    systemTypes = null;

                    systemAdded.Dispose();
                    systemRemoved.Dispose();

                    systemAdded = null;
                    systemRemoved = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
