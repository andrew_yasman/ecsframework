﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class ComponentType : IDisposable
    {
        private Type compType;
        private int bitIndex;
        private BitIdentification bitID;

        public ComponentType(int bit, Type compType)
        {
            bitIndex = bit;
            this.compType = compType;
        }

        public Type DataType => compType;
        public int BitIndex => bitIndex;
        public BitIdentification BitIdentification => bitID;

        public void CalculateBitID(int maxComponents)
        {
            bitID = new BitIdentification(maxComponents, BitIndex);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    bitID.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
