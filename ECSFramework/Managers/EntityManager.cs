﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace AYLib.ECSFramework
{
    internal class EntityManager : IDisposable
    {
        private Subject<ChangedComponentData> componentAdded = new Subject<ChangedComponentData>();
        private Subject<ChangedComponentData> componentRemoved = new Subject<ChangedComponentData>();
        private Subject<Entity> entityAdded = new Subject<Entity>();
        private Subject<Entity> entityRemoved = new Subject<Entity>();

        public IObservable<ChangedComponentData> WhenComponentAdded => componentAdded.AsObservable().Publish().RefCount();
        public IObservable<ChangedComponentData> WhenComponentRemoved => componentRemoved.AsObservable().Publish().RefCount();
        public IObservable<Entity> WhenEntityAdded => entityAdded.AsObservable().Publish().RefCount();
        public IObservable<Entity> WhenEntityRemoved => entityRemoved.AsObservable().Publish().RefCount();

        private readonly ComponentTypeManager componentTypeManager;
        private readonly IScheduler scheduler;

        private readonly State internalState;

        private StaticIndexList<StaticIndexList<IComponent>> componentData = new StaticIndexList<StaticIndexList<IComponent>>();
        private StaticIndexList<Entity> entities = new StaticIndexList<Entity>();

        public EntityManager(ComponentTypeManager componentTypeManager, IScheduler overrideScheduler, State internalState)
        {
            this.internalState = internalState;
            this.componentTypeManager = componentTypeManager;
            this.scheduler = overrideScheduler ?? new DefaultScheduler();

            this.componentTypeManager.WhenComponentTypeAdded.Subscribe(ct =>
            {
                for(int i=0; i< entities.Count; i++)
                {
                    entities.At(i).NewComponentTypeRegistered(this.componentTypeManager.ComponentCount);
                }
            });
        }

        public Entity CreateEntity()
        {
            Entity newEntity = new Entity(this, componentTypeManager.ComponentCount, entities.Next, true);
            entities.Insert(newEntity);

            entityAdded?.OnNext(newEntity);
            newEntity.IsEnabled = true;

            return newEntity;
        }

        internal void AddComponent(Entity entity, IComponent newComponent)
        {
            int componentTypeID = componentTypeManager.GetBitIndex(newComponent.GetType());
            if (componentTypeID == int.MinValue)
            {
                componentTypeManager.RegisterType(newComponent.GetType());
                componentTypeID = componentTypeManager.GetBitIndex(newComponent.GetType());
            }
                
            StaticIndexList<IComponent> componentSlot = componentData.At(componentTypeID);
            if (componentSlot == null)
            {
                componentSlot = new StaticIndexList<IComponent>();
                componentData.Insert(componentTypeID, componentSlot);
            }

            int entityID = entity.ID;
            componentSlot.Insert(entityID, newComponent);

            entity.SetComponentBit(componentTypeID);

            componentAdded?.OnNext(new ChangedComponentData(entity, newComponent));
        }

        internal void RemoveComponent<T>(Entity entity)
        {
            int componentTypeID = componentTypeManager.GetBitIndex(typeof(T));
            StaticIndexList<IComponent> componentSlot = componentData.At(componentTypeID);

            var oldComponent = componentSlot.RemoveAt(entity.ID);
            entity.UnsetComponentBit(componentTypeID);

            componentRemoved?.OnNext(new ChangedComponentData(entity, oldComponent));
        }

        internal void RemoveAllComponent(Entity entity)
        {
            foreach (var index in entity.AssignedComponents.GetBitIndexes())
            {
                StaticIndexList<IComponent> componentSlot = componentData.At(index);
                var removed = componentSlot.RemoveAt(entity.ID);
                entity.UnsetComponentBit(index);

                componentRemoved?.OnNext(new ChangedComponentData(entity, removed));
            }
        }

        internal void AddComponent<T>(Entity entity) where T : IComponent
        {
            IComponent newComponent = Activator.CreateInstance(typeof(T)) as IComponent;
            AddComponent(entity, newComponent);
        }

        public IEnumerable<IComponent> GetComponents(Entity entity)
        {
            if (entity != null)
            {
                foreach (var index in entity.AssignedComponents.GetBitIndexes())
                {
                    StaticIndexList<IComponent> componentSlot = componentData.At(index);
                    yield return componentSlot.At(index);
                }
            }
        }

        public IEnumerable<Entity> GetEntities(TypeFilter filter)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities.HasAt(i))
                {
                    var retVal = entities.At(i);

                    if (filter.Matches(retVal.AssignedComponents))
                    {
                        yield return entities.At(i);
                    }
                }
            }
        }

        public Entity GetEntity(int entityID)
        {
            return entities.At(entityID);
        }

        public void Remove(Entity entity)
        {
            if (entities.HasAt(entity.ID))
            {
                RemoveAllComponent(entity);
                entities.RemoveAt(entity.ID);

                entityRemoved?.OnNext(entity);
            }
        }

        public void Remove(int entityID)
        {
            if (entities.HasAt(entityID))
            {
                var entity = entities.At(entityID);

                RemoveAllComponent(entity);
                entities.RemoveAt(entity.ID);

                entityRemoved?.OnNext(entity);
            }
        }

        internal IComponent GetComponent(Entity entity, ComponentType componentType)
        {
            int componentTypeID = componentType.BitIndex;
            StaticIndexList<IComponent> componentSlot = componentData.At(componentTypeID);
            return componentSlot.At(entity.ID);
        }

        internal IComponent GetComponent<T>(Entity entity)
        {
            int componentTypeID = componentTypeManager.GetBitIndex(typeof(T));
            StaticIndexList<IComponent> componentSlot = componentData.At(componentTypeID);
            return componentSlot?.At(entity.ID);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    componentAdded.Dispose();
                    componentRemoved.Dispose();
                    entityAdded.Dispose();
                    entityRemoved.Dispose();

                    componentAdded = null;
                    componentRemoved = null;
                    entityAdded = null;
                    entityRemoved = null;

                    for (int i = 0; i < entities.Count; i++)
                        entities.At(i).Dispose();
                    entities.Dispose();
                    
                    for(int i=0; i< componentData.Count; i++)
                        componentData.At(i).Dispose();
                    componentData.Dispose();

                    entities = null;
                    componentData = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
