﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    public interface IScheduler
    {
        bool InUpdate { get; set; }

        void Add(Action pendingAction);

        void Synchronize();
    }
}
