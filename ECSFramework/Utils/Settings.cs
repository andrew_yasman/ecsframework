﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    public class Settings
    {
        public bool AllowRegistrationAfterInit { get; set; }

        public Settings(bool allowRegistrationAfterInit)
        {
            AllowRegistrationAfterInit = allowRegistrationAfterInit;
        }
    }
}
