﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    [SynchronizationPoint(SynchronizationPointAttribute.SyncPoint.None)]
    public class NullScheduler : IScheduler
    {
        public bool InUpdate { get; set; }

        public NullScheduler()
        {
            InUpdate = false;
        }

        public void Add(Action pendingAction)
        {
            pendingAction?.Invoke();
        }

        public void Synchronize()
        {

        }
    }
}
