﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class BitIdentification : IDisposable
    {
        private int[] bitMaps;
        private int dataSize = (sizeof(int) * 8);

        public bool Empty
        {
            get
            {
                for(int i=0; i<bitMaps.Length; i++)
                {
                    if (bitMaps[i] != 0)
                        return false;
                }
                return true;
            }
        }

        public BitIdentification(int initialMaxCount, params int[] indexes)
        {
            int mapsNeeded = (initialMaxCount / dataSize) + 1;

            bitMaps = new int[mapsNeeded];

            foreach (int bit in indexes)
            {
                SetBit(bit);
            }
        }

        internal bool NeedsResize(int index)
        {
            int zone = index / dataSize;
            return zone < bitMaps.Length;
        }

        internal void Resize(int newMaxCount)
        {
            int mapsNeeded = (newMaxCount / dataSize) + 1;
            int[] newMap = new int[mapsNeeded];
            
            for (int i=0; i< bitMaps.Length; i++)
                newMap[i] = bitMaps[i];
        }

        public void SetBit(int index)
        {
            int zone = index / dataSize;
            int setBit = (index % dataSize);
            bitMaps[zone] |= 1 << index;
        }

        public void UnsetBit(int index)
        {
            int zone = index / dataSize;
            int setBit = (index % dataSize);
            bitMaps[zone] &= ~(1 << index);
        }

        public IEnumerable<int> GetBitIndexes()
        {
            for (int i = 0; i < bitMaps.Length; i++)
            {
                int checkBitmap = bitMaps[i];
                int currentBit = 0;
                while(checkBitmap!=0)
                {
                    if ((1 & checkBitmap) == 1)
                    {
                        yield return currentBit + (i * dataSize);
                    }
                    checkBitmap = checkBitmap >> 1;
                    currentBit++;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Equal(BitIdentification componentID, BitIdentification fullID)
        {
            if (componentID.bitMaps.Length != fullID.bitMaps.Length)
                return false;

            for (int i = 0; i < bitMaps.Length; i++)
            {
                if (componentID.bitMaps[i] != fullID.bitMaps[i])
                    return false;
            }
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool All(BitIdentification fullID)
        {
            if (this.bitMaps.Length != fullID.bitMaps.Length)
                return false;

            for (int i = 0; i < bitMaps.Length; i++)
            {
                int curBitMap = this.bitMaps[i];
                if ((curBitMap & fullID.bitMaps[i]) != curBitMap)
                    return false;
            }
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Any(BitIdentification fullID)
        {
            if (this.bitMaps.Length != fullID.bitMaps.Length)
                return false;

            for (int i = 0; i < bitMaps.Length; i++)
            {
                if ((this.bitMaps[i] & fullID.bitMaps[i]) != 0)
                    return true;
            }
            return false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool None(BitIdentification fullID)
        {
            if (this.bitMaps.Length != fullID.bitMaps.Length)
                return false;

            for (int i = 0; i < bitMaps.Length; i++)
            {
                if ((this.bitMaps[i] & fullID.bitMaps[i]) != 0)
                    return false;
            }
            return true;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    bitMaps = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
