﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    [SynchronizationPoint(SynchronizationPointAttribute.SyncPoint.AfterUpdate)]
    public class DefaultScheduler : IScheduler
    {
        private Queue<Action> pendingActions = new Queue<Action>();

        public bool HasChanges { get; private set; }
        public bool InUpdate { get; set; }

        public DefaultScheduler()
        {
            HasChanges = false;
            InUpdate = false;
        }

        public void Add(Action pendingAction)
        {
            if (InUpdate)
            {
                pendingActions.Enqueue(pendingAction);
                HasChanges = true;
            }
            else
                pendingAction?.Invoke();
        }

        public void Synchronize()
        {
            if (HasChanges)
            {
                while (pendingActions.Count > 0)
                {
                    pendingActions.Dequeue()?.Invoke();
                }
                HasChanges = false;
            }
        }
    }
}
