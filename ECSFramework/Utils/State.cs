﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class State
    {
        private Settings EngineSettings { get; set; }

        public bool Initialized { get; set; }

        public bool SyncBeforeUpdate { get; private set; }
        public bool SyncAfterUpdate { get; private set; }

        public State(IScheduler scheduler, Settings overrideSettings = null)
        {
            Initialized = false;
            EngineSettings = overrideSettings ?? new Settings(true);

            SyncBeforeUpdate = true;
            SyncAfterUpdate = true;

            if (scheduler!=null)
            {
                var attrib = scheduler.GetType().GetCustomAttributes(typeof(SynchronizationPointAttribute), false).ToList().OfType<SynchronizationPointAttribute>().FirstOrDefault();
                if (attrib != null)
                {
                    if ((int)attrib.SyncPoints == 0)
                    {
                        SyncAfterUpdate = false;
                        SyncBeforeUpdate = false;
                    }
                    else
                    {
                        if ((SynchronizationPointAttribute.SyncPoint.AfterUpdate & attrib.SyncPoints) == 0)
                            SyncAfterUpdate = false;
                        if ((SynchronizationPointAttribute.SyncPoint.BeforeUpdate & attrib.SyncPoints) == 0)
                            SyncBeforeUpdate = false;
                    }
                }
            }
        }
    }
}
