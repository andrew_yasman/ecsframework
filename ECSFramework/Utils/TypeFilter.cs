﻿using System;
using System.Collections.Generic;

namespace AYLib.ECSFramework
{
    public class TypeFilter : IDisposable
    {
        private List<Type> tempAllTypes = new List<Type>();
        private List<Type> tempAnyTypes = new List<Type>();
        private List<Type> tempNoneTypes = new List<Type>();

        internal BitIdentification AllTypeFilter { get; private set; }

        internal BitIdentification AnyTypeFilter { get; private set; }

        internal BitIdentification NoneTypeFilter { get; private set; }

        internal TypeFilter()
        {

        }

        public static TypeFilter All(params Type[] types)
        {
            return new TypeFilter().GetAll(types);
        }

        public static TypeFilter Any(params Type[] types)
        {
            return new TypeFilter().GetAny(types);
        }

        public static TypeFilter None(params Type[] types)
        {
            return new TypeFilter().GetNone(types);
        }

        public static TypeFilter Empty()
        {
            return new TypeFilter();
        }

        public TypeFilter GetAll(params Type[] types)
        {
            tempAllTypes.AddRange(types);
            return this;
        }

        public TypeFilter GetAny(params Type[] types)
        {
            tempAnyTypes.AddRange(types);
            return this;
        }

        public TypeFilter GetNone(params Type[] types)
        {
            tempNoneTypes.AddRange(types);
            return this;
        }

        internal void CalculateFilterID(ComponentTypeManager componentTypeManager)
        {
            AllTypeFilter = new BitIdentification(componentTypeManager.ComponentCount);
            AnyTypeFilter = new BitIdentification(componentTypeManager.ComponentCount);
            NoneTypeFilter = new BitIdentification(componentTypeManager.ComponentCount);

            RegisterTypeAndSetBit(componentTypeManager, AllTypeFilter, ref tempAllTypes);
            RegisterTypeAndSetBit(componentTypeManager, AnyTypeFilter, ref tempAnyTypes);
            RegisterTypeAndSetBit(componentTypeManager, NoneTypeFilter, ref tempNoneTypes);

            tempAllTypes = null;
            tempAnyTypes = null;
            tempNoneTypes = null;
        }

        internal void Resize(int newMaxCount)
        {
            if (AllTypeFilter.NeedsResize(newMaxCount))
                AllTypeFilter.Resize(newMaxCount);
            if (AnyTypeFilter.NeedsResize(newMaxCount))
                AnyTypeFilter.Resize(newMaxCount);
            if (NoneTypeFilter.NeedsResize(newMaxCount))
                NoneTypeFilter.Resize(newMaxCount);
        }

        private void RegisterTypeAndSetBit(ComponentTypeManager componentTypeManager, BitIdentification bitID, ref List<Type> types)
        {
            if (types != null && types.Count > 0)
            {
                foreach (var t in types)
                {
                    if (componentTypeManager.GetType(t) == null)
                        componentTypeManager.RegisterType(t);

                    bitID.SetBit(componentTypeManager.GetBitIndex(t));
                }
            }
        }

        internal bool Matches(BitIdentification fullID)
        {
            if ((this.AllTypeFilter.All(fullID) || this.AllTypeFilter.Empty) &&
                (this.AnyTypeFilter.Any(fullID) || this.AnyTypeFilter.Empty) &&
                (this.NoneTypeFilter.None(fullID) || this.NoneTypeFilter.Empty))
                return true;
            return false;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    AllTypeFilter.Dispose();
                    AnyTypeFilter.Dispose();
                    NoneTypeFilter.Dispose();

                    AllTypeFilter = null;
                    AnyTypeFilter = null;
                    NoneTypeFilter = null;

                    tempAllTypes = null;
                    tempAnyTypes = null;
                    tempAnyTypes = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
