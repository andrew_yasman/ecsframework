﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AYLib.ECSFramework
{
    internal class StaticIndexList<T> : IDisposable
    {
        public Queue<int> emptyListSlots = new Queue<int>();
        public List<T> listData = new List<T>();

        public int Count => listData.Count;

        public int Next
        {
            get
            {
                if (emptyListSlots.Count > 0)
                    return emptyListSlots.Peek();
                return Count;
            }
        }

        public int Insert(T item)
        {
            int next = listData.Count;
            if (emptyListSlots.Count > 0)
            {
                next = emptyListSlots.Dequeue();
                listData[next] = item;
            }
            else
            {
                listData.Insert(next, item);
            }
            return next;
        }

        public int Insert(int index, T item)
        {
            if (index > listData.Count)
            {
                int rem = index - listData.Count;

                if (rem > 0)
                {
                    int start = listData.Count;

                    T[] newTempList = new T[index];
                    listData.CopyTo(newTempList);
                    listData = newTempList.ToList();

                    for (int i = start; i < index; i++)
                        emptyListSlots.Enqueue(i);
                }

                //while (rem > 0)
                //{
                //    int nextEntry = listData.Count;
                //    emptyListSlots.Enqueue(nextEntry);
                //    listData.Insert(nextEntry, default(T));
                //    rem--;
                //}
            }
            if (index >= listData.Count)
            {
                listData.Insert(index, item);
            }
            else
            {
                listData[index] = item;
            }
            return index;
        }

        public T RemoveAt(int index)
        {
            T retVal = listData[index];
            listData[index] = default(T);
            emptyListSlots.Enqueue(index);
            return retVal;
        }

        public T Find(Predicate<T> match)
        {
            return listData.Find(match);
        }

        public T At(int index)
        {
            if (!HasAt(index))
                return default(T);
            return listData[index];
        }

        public bool HasAt(int index)
        {
            if (index >= 0 && listData.Count > index && !EqualityComparer<T>.Default.Equals(listData[index], default(T)))
                return true;
            return false;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    emptyListSlots.Clear();
                    emptyListSlots = null;

                    listData.OfType<IDisposable>().ToList().ForEach(ld => ld.Dispose());
                    listData.Clear();
                    listData = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
