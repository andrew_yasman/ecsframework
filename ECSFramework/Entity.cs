﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    public sealed class Entity
    {
        private EntityManager entityManager;

        internal BitIdentification AssignedComponents { get; private set; }

        public int ID { get; internal set; }

        public bool IsEnabled { get; internal set; }

        internal Entity(EntityManager entityManager, int initialMaxComponents, int id, bool isEnabled)
        {
            this.entityManager = entityManager;
            this.ID = id;
            this.IsEnabled = isEnabled;
            AssignedComponents = new BitIdentification(initialMaxComponents);
        }

        public void AddComponent(IComponent newComponent)
        {
            entityManager.AddComponent(this, newComponent);
        }

        public void AddComponent<T>(T newComponent) where T : IComponent
        {
            entityManager.AddComponent(this, newComponent);
        }

        public void AddComponent<T>() where T : IComponent
        {
            entityManager.AddComponent<T>(this);
        }

        public void RemoveComponent<T>() where T : IComponent
        {
            entityManager.RemoveComponent<T>(this);
        }

        public IComponent GetComponent<T>() where T : IComponent
        {
            return entityManager.GetComponent<T>(this);
        }

        public void HasComponent<T>() where T : IComponent
        {

        }

        internal Dictionary<Type, IComponent> GetComponents()
        {
            Dictionary<Type, IComponent> retVal = new Dictionary<Type, IComponent>();
            foreach(var component in entityManager.GetComponents(this))
            {
                if (component!=null)
                retVal.Add(component.GetType(), component);
            }
            return retVal;
        }

        internal void SetComponentBit(int index)
        {
            AssignedComponents.SetBit(index);
        }

        internal void UnsetComponentBit(int index)
        {
            AssignedComponents.UnsetBit(index);
        }

        internal void NewComponentTypeRegistered(int newMaxTypes)
        {
            if (AssignedComponents.NeedsResize(newMaxTypes))
                AssignedComponents.Resize(newMaxTypes);
        }

        internal void Dispose()
        {
            AssignedComponents.Dispose();
            AssignedComponents = null;
        }
    }
}
