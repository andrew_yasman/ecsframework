﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AYLib.ECSFramework
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SynchronizationPointAttribute : Attribute
    {
        [Flags]
        public enum SyncPoint
        {
            None = 0,
            BeforeUpdate = 1,
            AfterUpdate = 2
        }

        public SyncPoint SyncPoints { get; private set; }

        public SynchronizationPointAttribute()
        {
            SyncPoints = SyncPoint.None;
        }

        public SynchronizationPointAttribute(SyncPoint syncPoints)
        {
            SyncPoints = syncPoints;
        }
    }
}
